package com.training2.lolchampion.service;

import com.training2.lolchampion.entities.LolChampion;
import com.training2.lolchampion.repository.ChampionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ChampionService {

    @Autowired
    ChampionRepository championRepository;

    public List<LolChampion> findAll() {
        return this.championRepository.findAll();
    }

    public LolChampion save(LolChampion lolChampion) {
        return this.championRepository.save(lolChampion);
    }

}
