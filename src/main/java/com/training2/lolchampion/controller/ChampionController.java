package com.training2.lolchampion.controller;

import com.training2.lolchampion.entities.LolChampion;
import com.training2.lolchampion.service.ChampionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping ("/api/v1/lolchampion")
public class ChampionController {

    private static final Logger LOG = LoggerFactory.getLogger(ChampionController.class);

    @Autowired
    private ChampionService championService;

    @GetMapping
    public List<LolChampion> findAll() {
        return this.championService.findAll();
    }

    @PostMapping
    public LolChampion save(@RequestBody LolChampion lolChampion) {
        LOG.debug("Request to create lolChampion [" + lolChampion + "]");
        return this.championService.save(lolChampion);
    }
}
