package com.training2.lolchampion.repository;

import com.training2.lolchampion.entities.LolChampion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface ChampionRepository extends JpaRepository <LolChampion, Long>  {
}
